package com.example.springsecurity.student;

public class Student {

    private Integer studentId;

    private String studentName;


    public Student() {}

    public Student(Integer studentId, String studentName) {
        this.studentId = studentId;
        this.studentName = studentName;
    }

    public Integer getStudentId() {
        return this.studentId;
    }

    public void setStudentId(Integer id) {
        this.studentId = id;
    }

    public String getStudentName() {
        return this.studentName;
    }


    public void setStudentName(String name) {
        this.studentName = name;
    }

}
